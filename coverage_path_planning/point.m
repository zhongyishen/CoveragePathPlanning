classdef point
    properties
        position % the coordinate of the point
        covered % if the point is covered or not
        x1 % self activity
        x2 % around effect activity
        x3 % direction effect activity
    end
    methods
        function obj = point(p, c, x, y, z)
            obj.position = p;
            obj.covered = c;
            obj.x1 = x;
            obj.x2 = y;
            obj.x3 = z;
        end
    end
end
