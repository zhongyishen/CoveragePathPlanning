%% Generate Map
clear;
close all;
map_size = 40;
obs_num = 8;
obstacles = map(map_size, obs_num);
%% Generate Path
[points, path] = binn(map_size, obstacles);
%% Draw Grid Moving Animation
draw_rec_animation(path);
%draw_path_animation(path);