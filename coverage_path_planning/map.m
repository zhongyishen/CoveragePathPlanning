function obstacles = map(map_size, obs_num)
figure;
box on;
% save the obstacles coordinate
% cow 1 is left-down x coordinate
% cow 2 is left-down y coordinate
% cow 3 is right-up x coordinate
% cow 4 is right-up y coordinate
obstacles = zeros(obs_num, 4);
for i = 1:obs_num
    length = floor(5 * rand) + 1; % [1,2,3,4,5]
    width = floor(4 * rand) + 1; % [1,2,3,4]
    x = floor((map_size - length) * rand);
    y = floor((map_size - width) * rand);
    rectangle('Position', [x,y,length,width], 'FaceColor', [0.3 0.3 0.3]);
    obstacles(i, 1) = x;obstacles(i, 3) = x + length;
    obstacles(i, 2) = y;obstacles(i, 4) = y + width;
end
for i = 0:map_size
    line([0,map_size], [i,i], 'Color', 'k');
    line([i,i], [0,map_size], 'Color', 'k');
end

