function draw_path(path)
hold on;
axis equal;
for i = 1:length(path)-1
    %rectangle('Position', [path(i).position(1,1),path(i).position(1,2),1,1], 'FaceColor', [0 .5 .5]);
    line([path(i).position(1,1) + 0.5, path(i+1).position(1,1) + 0.5],...
         [path(i).position(1,2) + 0.5, path(i+1).position(1,2) + 0.5],'Color', 'r');
    %pause(0.1);
end