function [path, current] = a_star(current_point,points,obstacles)
min_dis = size(points, 1).^2;
current_point.covered = 1;
target_point = point.empty;
around_points = point.empty;
last_position = [0,0];
path = point.empty;
path(end+1) = current_point;
count = 0;
for i = 1:size(points,1)
    for j = 1:size(points,2)
        if ~is_obstacle(points(i,j).position, obstacles) && points(i,j).covered == 0
            dis = sqrt((current_point.position(1,1) - points(i,j).position(1,1)).^2 + ...
                       (current_point.position(1,2) - points(i,j).position(1,2)).^2);
            if dis < min_dis
                min_dis = dis;
                target_point = points(i,j);
            end
        end
    end
end
fprintf("find a target[%d,%d],try to fetch...\n",target_point.position(1,1),target_point.position(1,2));
% | 1 | 2 | 3 |
% | 4 | # | 5 |
% | 6 | 7 | 8 |
while true
    % grid 1
    if in_map(current_point.position + [1,1] + [-1,1], size(points,1)) && ...
       ~is_obstacle(current_point.position + [-1,1], obstacles)
        around_points(end+1) = points(current_point.position(1,1), current_point.position(1,2) + 2);
    end
    % grid 2
    if in_map(current_point.position + [1,1] + [0,1], size(points,1)) && ...
       ~is_obstacle(current_point.position + [0,1], obstacles)
        around_points(end+1) = points(current_point.position(1,1) + 1, current_point.position(1,2) + 2);
    end
    % grid 3
    if in_map(current_point.position + [1,1] + [1,1], size(points,1)) && ...
       ~is_obstacle(current_point.position + [1,1], obstacles)
        around_points(end+1) = points(current_point.position(1,1) + 2, current_point.position(1,2) + 2);
    end
    % grid 4
    if in_map(current_point.position + [1,1] + [-1,0], size(points,1)) && ...
       ~is_obstacle(current_point.position + [-1,0], obstacles)
        around_points(end+1) = points(current_point.position(1,1), current_point.position(1,2) + 1);
    end
    % grid 5
    if in_map(current_point.position + [1,1] + [1,0], size(points,1)) && ...
       ~is_obstacle(current_point.position + [1,0], obstacles)
        around_points(end+1) = points(current_point.position(1,1) + 2, current_point.position(1,2) + 1);
    end
    % grid 6
    if in_map(current_point.position + [1,1] + [-1,-1], size(points,1)) && ...
       ~is_obstacle(current_point.position + [-1,-1], obstacles)
        around_points(end+1) = points(current_point.position(1,1), current_point.position(1,2));
    end
    % grid 7
    if in_map(current_point.position + [1,1] + [0,-1], size(points,1)) && ...
       ~is_obstacle(current_point.position + [0,-1], obstacles)
        around_points(end+1) = points(current_point.position(1,1) + 1, current_point.position(1,2));
    end
    % grid 8
    if in_map(current_point.position + [1,1] + [1,-1], size(points,1)) && ...
       ~is_obstacle(current_point.position + [1,-1], obstacles)
        around_points(end+1) = points(current_point.position(1,1) + 2, current_point.position(1,2));
    end
    if ~isempty(around_points)
        for i = 1:length(around_points)
            if around_points(i).position == last_position
                around_points(i) = [];
                break;
            end
        end
        fprintf("around points >>\n");
        f_min = a_star_function(current_point, around_points(1), target_point);
        min_index = 1;
        for i = 1:length(around_points)
            fprintf("[%d,%d]\n",around_points(i).position(1,1),around_points(i).position(1,2));
            f = a_star_function(current_point, around_points(i), target_point);
            if f < f_min
                f_min = f;
                min_index = i;
            end
        end
        last_position = current_point.position;
        current_point = around_points(min_index);
        current_point.covered = 1;
        path(end+1) = current_point;
        fprintf("choose [%d,%d] as next point\n", current_point.position(1,1),current_point.position(1,2));
    else
        fprintf("around points are all unaccessible\n");
        break;
    end
    around_points = point.empty;
    count = count + 1;
    if count == 3000
        current = current_point;
        fprintf("time is up, fail to arrive target\n");
        break;
    end
    if current_point.position == target_point.position
        current = target_point;
        fprintf("arrive target\n");
        break;
    end
end


