function bool = all_covered(points)
bool = 1;
for i = 1:size(points,1)
	for j = 1:size(points,2)
		if points(i,j).covered == 0
			bool = 0;
		end
	end
end