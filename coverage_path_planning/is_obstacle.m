function bool = is_obstacle(position, obstacles)
for i = 1:size(obstacles,1)
    if position(1,1) >= obstacles(i,1) && ...
       position(1,2) >= obstacles(i,2) && ...
       position(1,1) < obstacles(i,3)  && ...
       position(1,2) < obstacles(i,4)
        bool = 1;
        break;
    else
        bool = 0;
    end
end
