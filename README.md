# CoveragePathPlanning

#### 介绍
CoveragePathPlanning using BINN with MATLAB

#### 效果
#### map_size = 10*10
![输入图片说明](https://images.gitee.com/uploads/images/2020/0722/123437_80fe2937_7731137.png "map_10.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0722/123447_5e6d5930_7731137.png "covered_map_10.png")
#### map_size = 20*20
![输入图片说明](https://images.gitee.com/uploads/images/2020/0722/114859_121e9195_7731137.png "map.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0722/114912_27e44a24_7731137.png "covered_map.png")
#### map_size = 30*30
![输入图片说明](https://images.gitee.com/uploads/images/2020/0722/123455_cac0b361_7731137.png "map_30.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0722/123503_d95a9c78_7731137.png "covered_map_30.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0722/221042_6f3ae44a_7731137.png "map.png")
#### 使用说明
map.m 可以生成指定大小和障碍物数量的随机地图<br>
manifest.m 可以动态绘制出路径<br>
main.mlx 直接查看最后生成的路径